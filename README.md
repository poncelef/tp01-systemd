# TP01-Systemd

## First-Step


🌞 systemd est bien PID 1 :

<pre>[poncelef@localhost ~]$ ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         <strong>1</strong>     0  0 10:21 ?        00:00:02 <strong>/usr/lib/systemd/systemd</strong>  --switched-root --system --deserialize 33 
root         2     0  0 10:21 ?        00:00:00 [kthreadd]
root         3     2  0 10:21 ?        00:00:00 [rcu_gp]
root         4     2  0 10:21 ?        00:00:00 [rcu_par_gp]
</pre>

🌞 Les process kernel portent le PPID 2, il suffit d'afficher tous les process qui n'ont pas le PPID 2 :

<pre>[poncelef@localhost ~]$ ps -u poncelef p 2 -o uname,pid,ppid,cmd,cls --deselect
USER       PID  PPID CMD                         CLS
root         1     0 /usr/lib/systemd/systemd --  TS
root         3     2 [rcu_gp]                     TS
root         4     2 [rcu_par_gp]                 TS
root         6     2 [kworker/0:0H-kblockd]       TS
root         9     2 [mm_percpu_wq]               TS
root        10     2 [ksoftirqd/0]                TS
root        11     2 [rcu_sched]                  TS
root        12     2 [migration/0]                FF
root        13     2 [cpuhp/0]                    TS
root        14     2 [cpuhp/1]                    TS
root        15     2 [migration/1]                FF
root        16     2 [ksoftirqd/1]                TS
root        18     2 [kworker/1:0H-kblockd]       TS
root        19     2 [kdevtmpfs]                  TS
root        20     2 [netns]                      TS
root        21     2 [rcu_tasks_kthre]            TS
root        22     2 [kauditd]                    TS
root        23     2 [oom_reaper]                 TS
root        24     2 [writeback]                  TS
root        25     2 [kcompactd0]                 TS
root        26     2 [ksmd]                       TS
root        27     2 [khugepaged]                 TS
root        50     2 [cryptd]                     TS
root       131     2 [kintegrityd]                TS
root       132     2 [kblockd]                    TS
root       133     2 [blkcg_punt_bio]             TS
root       134     2 [tpm_dev_wq]                 TS
root       135     2 [ata_sff]                    TS
root       136     2 [md]                         TS
root       137     2 [edac-poller]                TS
root       138     2 [devfreq_wq]                 TS
root       139     2 [watchdogd]                  FF
root       142     2 [kswapd0]                    TS
root       143     2 [kworker/u5:0]               TS
root       146     2 [kthrotld]                   TS
root       147     2 [acpi_thermal_pm]            TS
root       148     2 [scsi_eh_0]                  TS
root       149     2 [scsi_tmf_0]                 TS
root       150     2 [scsi_eh_1]                  TS
root       151     2 [scsi_tmf_1]                 TS
root       152     2 [scsi_eh_2]                  TS
root       153     2 [scsi_tmf_2]                 TS
root       157     2 [dm_bufio_cache]             TS
root       158     2 [ipv6_addrconf]              TS
root       165     2 [kstrp]                      TS
root       241     2 [kworker/1:1H-kblockd]       TS
root       242     2 [kworker/0:1H-kblockd]       TS
root       454     2 [irq/18-vmwgfx]              FF
root       455     2 [ttm_swap]                   TS
root       508     2 [kdmflush]                   TS
root       515     2 [kdmflush]                   TS
root       534     2 [jbd2/dm-0-8]                TS
root       535     2 [ext4-rsv-conver]            TS
root       622     1 /usr/lib/systemd/systemd-jo  TS
root       647     1 /usr/lib/systemd/systemd-ud  TS
root       741     2 [jbd2/sda1-8]                TS
root       742     2 [ext4-rsv-conver]            TS
root       763     1 /sbin/auditd                 TS
root       774     2 [rpciod]                     TS
root       775     2 [xprtiod]                    TS
root       784     1 /usr/bin/python3 /usr/sbin/  TS
root       785     1 /usr/sbin/ModemManager       TS
root       786     1 /usr/lib/systemd/systemd-ma  TS
root       787     1 /usr/sbin/VBoxService -f     TS
root       788     1 /usr/libexec/udisks2/udisks  TS
root       790     1 /usr/sbin/sssd -i --logger=  TS
rtkit      793     1 /usr/libexec/rtkit-daemon    TS
root       794     1 /usr/sbin/mcelog --ignoreno  TS
avahi      803     1 avahi-daemon: running [linu  TS
root       804     1 /sbin/rngd -f                TS
root       808     1 /usr/sbin/gssproxy -D        TS
dbus       828     1 /usr/bin/dbus-broker-launch  TS
chrony     833     1 /usr/sbin/chronyd            TS
root       841     1 /usr/sbin/alsactl -s -n 19   TS
root       845     1 /usr/sbin/abrtd -d -s        TS
dbus       847   828 dbus-broker --log 4 --contr  TS
avahi      852   803 avahi-daemon: chroot helper  TS
root       853   790 /usr/libexec/sssd/sssd_be -  TS
root       857     1 /usr/bin/abrt-dump-journal-  TS
root       858   790 /usr/libexec/sssd/sssd_nss   TS
root       859     1 /usr/bin/abrt-dump-journal-  TS
root       860     1 /usr/bin/abrt-dump-journal-  TS
polkitd    861     1 /usr/lib/polkit-1/polkitd -  TS
root       889     1 /usr/lib/systemd/systemd-lo  TS
root       897     1 /usr/libexec/accounts-daemo  TS
root       922     1 /usr/sbin/NetworkManager --  TS
root       945     1 /usr/sbin/cupsd -l           TS
root       952     1 /usr/sbin/libvirtd           TS
root       961     1 /usr/sbin/atd -f             TS
root       962     1 /usr/sbin/crond -n           TS
root      1063   922 /sbin/dhclient -d -q -sf /u  TS
dnsmasq   1153     1 /usr/sbin/dnsmasq --conf-fi  TS
root      1154  1153 /usr/sbin/dnsmasq --conf-fi  TS
root      1245     1 VBoxClient --vmsvga          TS
root      1254     1 /usr/sbin/gdm                TS
root      1405     1 /usr/libexec/upowerd         TS
root      1428     1 /usr/sbin/wpa_supplicant -c  TS
colord    1514     1 /usr/libexec/colord          TS
root      1595  1254 gdm-session-worker [pam/gdm  TS
root      1881     1 /usr/libexec/sssd/sssd_kcm   TS
root      2165     1 /usr/sbin/abrt-dbus -t133    TS
root      2279     1 /usr/libexec/fwupd/fwupd     TS
root      2286     1 /usr/libexec/boltd           TS
root      3880     2 [kworker/u4:0-events_unboun  TS
root      4227     2 [kworker/0:0-events]         TS
root      4387     2 [kworker/1:3-events_power_e  TS
root      4483     2 [kworker/0:1-events]         TS
root      4527     2 [kworker/u4:1-events_unboun  TS
root      4615     2 [kworker/1:1-ata_sff]        TS
root      4652     2 [kworker/u4:2-events_unboun  TS
root      4707     2 [kworker/1:0-ata_sff]        TS
</pre>

## Gestion du temps

🌞 Local time : heure syncornisé sur le fuseau horaire (CET +1 pour ma part)

Universal time : heure synconisé sur le fuseau horaire UTC 0

RTC time : heure syncronisé sur le hardware (maintenue par le Bios)

<pre>[poncelef@localhost ~]$ timedatectl
               Local time: ven. 2019-11-29 11:46:29 CET
           Universal time: ven. 2019-11-29 10:46:29 UTC
                 RTC time: ven. 2019-11-29 10:46:25
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
</pre>

🌞 Set timezone sur Europe/London :
<pre>[poncelef@localhost ~]$ timedatectl set-timezone Europe/London
[poncelef@localhost ~]$ timedatectl
               Local time: ven. 2019-11-29 11:16:20 GMT
           Universal time: ven. 2019-11-29 11:16:20 UTC
                 RTC time: ven. 2019-11-29 11:16:16
                <strong>Time zone: Europe/London (GMT, +0000)</strong>
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
</pre>

🌞 Désactivation de la syncronisation NTP :
*On peut voir qu'il à bien était désactivé, NTP service : inactive*
<pre>[poncelef@localhost ~]$ timedatectl set-ntp 0
[poncelef@localhost ~]$ timedatectl
               Local time: ven. 2019-11-29 11:19:00 GMT
           Universal time: ven. 2019-11-29 11:19:00 UTC
                 RTC time: ven. 2019-11-29 11:18:56
                Time zone: Europe/London (GMT, +0000)
System clock synchronized: yes
              <strong>NTP service: inactive</strong>
          RTC in local TZ: no
</pre>

## Gestion de noms

🌞 Le nom définie avec --pretty permet une identification plus simple pour l'utilisateur, il n'y a autant de restriction que pour le nom --static.
Le nom définie avec --static est le nom utiliser pour l'identification sur le réseau.
Le "--transient hostname" est un nom maintenue par le noyau, c'est un nom dynamique.

🌞 Pour les machines de production il est utile de renseigner le --static hostname pour les identifiers sur le réseau.
<pre>[poncelef@vm-fedora ~]$ hostnamectl set-hostname &quot;vmfedora&quot;

[poncelef@vm-fedora ~]$ hostnamectl set-hostname &quot;Machine virtuelle de Florentin&quot; --pretty

[poncelef@vm-fedora ~]$ hostnamectl status
   Static hostname: vmfedora
   Pretty hostname: Machine virtuelle de Florentin
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 65c1d563650b432cb707de9194ddf90a
           Boot ID: c8fb81f50e1847ceb86db800e56e824b
    Virtualization: oracle
  Operating System: Fedora 31 (Workstation Edition)
       CPE OS Name: cpe:/o:fedoraproject:fedora:31
            Kernel: Linux 5.3.12-300.fc31.x86_64
      Architecture: x86-64
</pre>

## Gestion du réseau

### NetworkManager

Dans mon cas c'est VirtualBox qui héberge son propre serveur DHCP, il possède l'adresse 10.0.0.2 et on peut voir qu'il a donné l'adresse 10.0.0.15 à la VM.

🌞 Toutes les options DHCP afficher par NetworkManager sont lister ici:
<pre>
IP4.ADDRESS[1]:                         10.0.2.15/24
IP4.GATEWAY:                            10.0.2.2
IP4.ROUTE[1]:                           dst = 0.0.0.0/0, nh = 10.0.2.2, mt = 100
IP4.ROUTE[2]:                           dst = 10.0.2.0/24, nh = 0.0.0.0, mt = 100
IP4.DNS[1]:                             192.168.1.1
IP4.DOMAIN[1]:                          home
DHCP4.OPTION[1]:                        dhcp_lease_time = 86400
DHCP4.OPTION[2]:                        dhcp_rebinding_time = 75600
DHCP4.OPTION[3]:                        dhcp_renewal_time = 43200
DHCP4.OPTION[4]:                        dhcp_server_identifier = 10.0.2.2
DHCP4.OPTION[5]:                        domain_name = home
DHCP4.OPTION[6]:                        domain_name_servers = 192.168.1.1
DHCP4.OPTION[7]:                        expiry = 1575640362
DHCP4.OPTION[8]:                        ip_address = 10.0.2.15
DHCP4.OPTION[9]:                        next_server = 10.0.2.4
DHCP4.OPTION[10]:                       requested_broadcast_address = 1
DHCP4.OPTION[11]:                       requested_dhcp_server_identifier = 1
DHCP4.OPTION[12]:                       requested_domain_name = 1
DHCP4.OPTION[13]:                       requested_domain_name_servers = 1
DHCP4.OPTION[14]:                       requested_domain_search = 1
DHCP4.OPTION[15]:                       requested_host_name = 1
DHCP4.OPTION[16]:                       requested_interface_mtu = 1
DHCP4.OPTION[17]:                       requested_ms_classless_static_routes = 1
DHCP4.OPTION[18]:                       requested_nis_domain = 1
DHCP4.OPTION[19]:                       requested_nis_servers = 1
DHCP4.OPTION[20]:                       requested_ntp_servers = 1
DHCP4.OPTION[21]:                       requested_rfc3442_classless_static_routes = 1
DHCP4.OPTION[22]:                       requested_root_path = 1
DHCP4.OPTION[23]:                       requested_routers = 1
DHCP4.OPTION[24]:                       requested_static_routes = 1
DHCP4.OPTION[25]:                       requested_subnet_mask = 1
DHCP4.OPTION[26]:                       requested_time_offset = 1
DHCP4.OPTION[27]:                       requested_wpad = 1
DHCP4.OPTION[28]:                       routers = 10.0.2.2
DHCP4.OPTION[29]:                       subnet_mask = 255.255.255.0
</pre>

🌞 Après avoir taper la commande "systemctl stop NetworkManager.service" le NetworkManager est désactivé

<pre>poncelef@vm-fedora ~]$ systemctl status NetworkManager.service
● NetworkManager.service - Network Manager
   Loaded: loaded (/usr/lib/systemd/system/NetworkManager.service; enabled; vendor preset: enabled)
   Active: inactive (dead) since Thu 2019-12-05 14:45:55 GMT; 41s ago</pre>
   
🌞 Pour activer Systemd networkd il faut taper la commande : systemctl start systemd-networkd

Pour activer systemd-networkd au boot, j'ai tapé la commande : systemd enable systemd-networkd


* 🌞 Après la création du fichier lan0.network et le rédémarrage du system : 

<pre>
[Match]
Name=enp0s3

[Network]
Address=192.168.1.20/24
Gateway=192.168.1.1
DNS=192.168.1.1
</pre>

* 🌞 J'ai pu constater la modification des paramètres du réseau : 

<pre>[poncelef@vmfedora ~]$ ifconfig

enp0s3: flags=4163&lt;UP,BROADCAST,RUNNING,MULTICAST&gt;  mtu 1500

        inet 192.168.1.20  netmask 255.255.255.0  broadcast 192.168.1.255
        ether aa:ee:dd:00:11:22  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 44  bytes 3383 (3.3 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
</pre>

### Resolved

🌞 Activation de resolved : systemctl start systemd-resolved.service
Activation au boot de resolved : enable systemd-resolved.service

<pre>
[poncelef@vmfedora ~]$ systemctl start systemd-resolved.service

[poncelef@vmfedora ~]$ systemctl enable systemd-resolved.service
Created symlink /etc/systemd/system/dbus-org.freedesktop.resolve1.service → /usr/lib/systemd/system/systemd-resolved.service.
Created symlink /etc/systemd/system/multi-user.target.wants/systemd-resolved.service → /usr/lib/systemd/system/systemd-resolved.service.
</pre>

🌞 Vérification d'écoute d'un port sur l'interface localhost :

<pre>
[poncelef@vmfedora ~]$ netstat -paunt
Connexions Internet actives (serveurs et établies)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat        PID/Program name    
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      3708/systemd-resolv 
</pre>

🌞 Requete DNS avec Resolved :

<pre>
[poncelef@vmfedora ~]$ resolvectl query gitlab.com
gitlab.com: <b>35.231.145.151</b>                     <font color="#8A8A8A">-- link: enp0s3</font>

<font color="#8A8A8A">-- Information acquired via protocol DNS in 58.5ms.</font>
<font color="#8A8A8A">-- Data is authenticated: no</font>
</pre>

<pre>[poncelef@vmfedora ~]$ dig gitlab.com @127.0.0.53
; &lt;&lt;&gt;&gt; DiG 9.11.13-RedHat-9.11.13-2.fc31 &lt;&lt;&gt;&gt; gitlab.com @127.0.0.53
;; global options: +cmd
;; Got answer:
;; -&gt;&gt;HEADER&lt;&lt;- opcode: QUERY, status: NOERROR, id: 59115
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494

;; QUESTION SECTION:
;gitlab.com.			IN	A

;; ANSWER SECTION:
gitlab.com.		62	IN	A	35.231.145.151

;; Query time: 7 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: mer. déc. 11 10:37:26 GMT 2019
;; MSG SIZE  rcvd: 55
</pre>

🌞 Modification du fichier resolv.conf :

<pre>
[Resolve]
DNS=172.0.0.53 1.1.1.1 1.0.0.1
</pre>

<pre>[poncelef@vmfedora systemd]$ resolvectl
<b>Global</b>
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
  Current DNS Server: 1.1.1.1
         DNS Servers: 1.1.1.1
                      1.0.0.1
                      172.0.0.53
</pre>

## Boot et Logs

🌞 Générer graphique de séquence de boot :
   
[Boot sequence](https://svgshare.com/i/Gjv.svg)






